import supertest from "supertest";
import { expect } from "chai";

const testApi = supertest("https://jsonplaceholder.typicode.com");

const post = {
  title: "foo",
  body: "bar",
  userId: 1,
};

describe("PUT API", () => {
  it("should return a 200 response on PUT", async () => {
    const id = 1;
    const response = await testApi
      .put(`/posts/${id}`)
      .set("Content-Type", "application/json; charset=UTF-8")
      .send(post)
      .then((response) => {
        return response;
      });

    expect(response.statusCode).to.be.equal(200);
  });

  it("should return updated post in response on PUT", async () => {
    const id = 1;
    const response = await testApi
      .put(`/posts/${id}`)
      .set("Content-Type", "application/json; charset=UTF-8")
      .send(post)
      .then((response) => {
        return response;
      });

    expect(response.body.userId).to.be.equal(1);
    expect(response.body.id).to.be.equal(1);
    expect(response.body.title).to.be.equal("foo");
    expect(response.body.body).to.be.equal("bar");
  });
});
