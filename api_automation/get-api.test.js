import supertest from "supertest";
import { expect } from "chai";

const testApi = supertest("https://jsonplaceholder.typicode.com");

describe("GET API", () => {
  it("should return a 200 response on GET", async () => {
    const response = await testApi.get("/posts").then((response) => {
      return response;
    });

    expect(response.statusCode).to.be.equal(200);
  });

  it("should return 100 posts in response on GET", async () => {
    const response = await testApi.get("/posts").then((response) => {
      return response;
    });

    expect(response.body.length).to.be.equal(100);
  });

  it("should return a single post in response on GET post/id", async () => {
    const id = 1;
    const response = await testApi.get(`/posts/${id}`).then((response) => {
      return response;
    });

    expect(response.body.userId).to.be.equal(1);
    expect(response.body.id).to.be.equal(1);
    expect(response.body.title).to.be.equal(
      "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"
    );
    expect(response.body.body).to.be.equal(
      "quia et suscipit\n" +
        "suscipit recusandae consequuntur expedita et cum\n" +
        "reprehenderit molestiae ut ut quas totam\n" +
        "nostrum rerum est autem sunt rem eveniet architecto"
    );
  });

  it("should get all posts and return the comments of first post in response", async () => {
    const postsApiResponse = await testApi.get("/posts").then((response) => {
      return response;
    });

    const post = postsApiResponse.body[0];
    const commentsResponse = await testApi
      .get(`/posts/${post.id}/comments`)
      .then((response) => {
        return response;
      });

    expect(commentsResponse.statusCode).to.be.equal(200);
    expect(commentsResponse.body.length).to.be.equal(5);
  });
});
