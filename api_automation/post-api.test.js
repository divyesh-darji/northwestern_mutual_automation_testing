import supertest from "supertest";
import { expect } from "chai";

const testApi = supertest("https://jsonplaceholder.typicode.com");

const post = {
  title: "foo",
  body: "bar",
  userId: 1,
};

describe("POST API", () => {
  it("should return a 201 response on POST", async () => {
    const response = await testApi
      .post("/posts")
      .set("Content-Type", "application/json; charset=UTF-8")
      .send(post)
      .then((response) => {
        return response;
      });

    expect(response.statusCode).to.be.equal(201);
  });

  it("should return created post in response on POST", async () => {
    const response = await testApi
      .post("/posts")
      .set("Content-Type", "application/json; charset=UTF-8")
      .send(post)
      .then((response) => {
        return response;
      });

    expect(response.body.title).to.be.equal("foo");
    expect(response.body.body).to.be.equal("bar");
    expect(response.body.userId).to.be.equal(1);
    expect(response.body.id).to.be.equal(101);
  });
});
