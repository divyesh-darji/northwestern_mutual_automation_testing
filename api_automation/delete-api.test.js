import supertest from "supertest";
import { expect } from "chai";

const testApi = supertest("https://jsonplaceholder.typicode.com");

describe("DELETE API", () => {
  it("should return a 200 response on DELETE", async () => {
    const id = 1;
    const response = await testApi.delete(`/posts/${id}`).then((response) => {
      return response;
    });

    expect(response.statusCode).to.be.equal(200);
  });
});
