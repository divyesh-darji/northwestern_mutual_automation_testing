# Northwestern Mutual Automation Testing

Task 1: A testing suite that comprehensively tests all features available at [SauceDemo](https://saucedemo.com). We are using webdriverio and mocha framework to automate the test cases.

I have also **created a Test Plan file** which is available in the repository which ensures the total coverage of the features and functionality of the website

Make sure you have node.js installed in your system.
If you do not have node.js installed then install Node.js with version v18.16.0 on your system by visiting [Node.js](https://nodejs.org/en/download)

Follow the below steps to run the code:
1. Clone the repository in your local machine and run the command **npm install**
2. Copy the .env.example file and rename it to .env file and enter the credentials
3. Once it is successfull run command **npm run wdio** to run the test cases
4. If you want to run a single spec file just to test a single feature functionality then run command **npm run wdio -- --spec ./test/specs/<file_name>** 

You will see that the test cases starts executing locally on your machine.

There were few bugs that I found while testing the website and I am mentioning it here:
1. The checkout button works even if the cart is empty. It will take you to the next page to fill the details for the user
2. After we add the products to the cart, we can skip the checkout-step-one page by directly hitting the url https://www.saucedemo.com/checkout-step-two.html to directly go and finish the purchase without entering first name, last name and zip code
3. While validating the zip code, I found that the zip code takes any arbitary input

Task 2: A testing suite that comprehensively performs API testing against [JsonPlaceHolder](https://jsonplaceholder.typicode.com/). Here I have performed API automation testing by making GET, POST, PUT and DELETE calls. We have used javascript and mocha framework to test the API's.

Follow the below steps to run the code:
1. If you didnt execute task 1 then install the dependencies by using **npm install** command.
2. Run the **npm test** command to execute the API automation test cases

Just to add a note, I have tried integrating it with the CICD pipeline and have a branch named CICDPipeline alloted for testing it but it didn't work out due to some errors.

The test plan have multiple test scenarios that I thought should be tested for the SauceDemo website. I have implemented the features on the login page, order page, product description page and the sorting feature on the inventory page. 