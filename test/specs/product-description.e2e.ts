import LoginPage from "../pageobjects/login.page.js";
import InventoryPage from "../pageobjects/inventory.page.js";
import ProductDescriptionPage from "../pageobjects/product-description.page.js";
import CartPage from "../pageobjects/cart.page.js";

const getCartItems = async () => {
  const inventoryItems = await CartPage.cartItems;

  const items = await Promise.all(
    inventoryItems.map(async (item) => ({
      quantity: await item.$(".cart_quantity").getText(),
      itemName: await item.$(".inventory_item_name").getText(),
      itemPrice: await item.$(".inventory_item_price").getText(),
      canonicalName: (await item.$(".inventory_item_name").getText())
        .toLowerCase()
        .replaceAll(" ", "-"),
    }))
  );

  return items;
};
/*
 * These are test cases for the product description page.
 */
describe("product-description", () => {
  /*
   * This is a hook that will run before each test.
   */
  beforeEach("doLogin", async () => {
    await LoginPage.open();

    await LoginPage.login(
      process.env.STANDARD_USER_NAME,
      process.env.STANDARD_USER_PASSWORD
    );

    await InventoryPage.open();
  });

  /*
   * This is a test case to open the product description page for a particular product.
   */
  describe("when user clicks on a product and it opens up the product description page for that particular product", () => {
    it("should display the correct output Sauce Labs Backpack", async () => {
      const myButton = await $("=Sauce Labs Backpack");
      await myButton.click();
      await expect(ProductDescriptionPage.productDescriptionTitle).toHaveText(
        "Sauce Labs Backpack"
      );
    });
  });

  /*
   * This is a test case to add the product to the cart when user clicks the add to cart button.
   */
  describe("when user clicks on add to cart button on Product description page", () => {
    beforeEach(async () => {
      const myButton = await $("=Sauce Labs Backpack");
      await myButton.click();
      await ProductDescriptionPage.addItemToCart("sauce-labs-backpack");
      const btnCart = await $(".shopping_cart_link");
      await btnCart.click();
    });

    it("should add the product in the cart", async () => {
      await expect(CartPage.cartItems).toBeElementsArrayOfSize(1);
      const cartItems = await getCartItems();
      expect(cartItems[0].itemName).toEqual("Sauce Labs Backpack");
    });
  });

  /*
   * This is a test case to remove the product from the cart when user clicks on remove button.
   */
  describe("when user clicks on remove button on Product description page", () => {
    beforeEach(async () => {
      await InventoryPage.open();
      const myButton = await $("=Sauce Labs Backpack");
      await myButton.click();
      await ProductDescriptionPage.removeFromCart("sauce-labs-backpack");
      const btnCart = await $(".shopping_cart_link");
      await btnCart.click();
    });

    it("should remove the product from the cart", async () => {
      await expect(CartPage.cartItems).toBeElementsArrayOfSize(0);
    });
  });
  /*
   * This is a test case to check if the user is able to navigate back to the inventory page when user clicks on back to products button.
   */
  describe("when user clicks on back to products button on Product description page", () => {
    beforeEach(async () => {
      await InventoryPage.open();
      const myButton = await $("=Sauce Labs Backpack");
      await myButton.click();
      await ProductDescriptionPage.backToProducts();
    });

    it("should take the user to the inventory page", async () => {
      await expect(InventoryPage.inventoryItems).toBeDisplayed();
    });
  });
});
