import LoginPage from "../pageobjects/login.page.js";
import OrderSummaryPage from "../pageobjects/order-summary.page.js";
import InventoryPage from "../pageobjects/inventory.page.js";
import { Element } from "webdriverio";

const TAX_PERCENT = 0.08;

/*
 * This is a method to get all the inventory items from the Inventory page
 */
const getInventoryItems = async () => {
  const inventoryItems = await InventoryPage.inventoryItems;

  const items = await Promise.all(
    inventoryItems.map(async (item) => ({
      itemName: await item.$(".inventory_item_name").getText(),
      itemPrice: await item.$(".inventory_item_price").getText(),
      canonicalName: (await item.$(".inventory_item_name").getText())
        .toLowerCase()
        .replaceAll(" ", "-"),
    }))
  );

  return items;
};

/*
 * This is a method to get all the items from the order summary page
 */
const getCartItems = async () => {
  const inventoryItems = await OrderSummaryPage.cartItems;

  const items = await Promise.all(
    inventoryItems.map(async (item) => ({
      quantity: await item.$(".cart_quantity").getText(),
      itemName: await item.$(".inventory_item_name").getText(),
      itemPrice: await item.$(".inventory_item_price").getText(),
      canonicalName: (await item.$(".inventory_item_name").getText())
        .toLowerCase()
        .replaceAll(" ", "-"),
    }))
  );

  return items;
};

const getItemTotal = async (itemTotalELement: ChainablePromiseElement) => {
  const itemTotal = await itemTotalELement.getText();
  return parseFloat(itemTotal.replaceAll("Item total: $", ""));
};

const getTax = async (taxElement: ChainablePromiseElement) => {
  const tax = await taxElement.getText();
  return parseFloat(tax.replaceAll("Tax: $", ""));
};

const getTotal = async (totalElement: ChainablePromiseElement) => {
  const total = await totalElement.getText();
  return parseFloat(total.replaceAll("Total: $", ""));
};

/*
 * These are test cases for the order summary page.
 */
describe("order-summary", () => {
  describe("when user opens inventory page", () => {
    let inventoryItems = null;
    before("doLogin", async () => {
      await LoginPage.open();

      await LoginPage.login(
        process.env.STANDARD_USER_NAME,
        process.env.STANDARD_USER_PASSWORD
      );

      await InventoryPage.open();
      inventoryItems = await getInventoryItems();
    });
    [1, 2, 3].forEach((numItems) => {
      describe(`and adds ${numItems} item(s) to cart`, () => {
        let selectedItems = null;
        before("add item to cart", async () => {
          selectedItems = inventoryItems.slice(0, numItems);
          for (const item of selectedItems) {
            await InventoryPage.addItemToCart(item.canonicalName);
          }
          await OrderSummaryPage.open();
        });

        /*
         * This is a test case to verify that there are items present in the cart.
         */
        it("shows an item in the cart", async () => {
          await expect(OrderSummaryPage.cartItems).toBeElementsArrayOfSize(
            numItems
          );
        });

        /*
         * This is a test case to verify that appropriate data and quantity for the product is shown in the order summary page.
         */
        it("has appropriate data and quantity", async () => {
          const cartItems = await getCartItems();
          const cartItemNames = cartItems.map((item) => item.itemName);
          for (const item of selectedItems) {
            expect(cartItemNames).toContain(item.itemName);
            const cartItem = cartItems.find(
              (cartItem) => cartItem.itemName === item.itemName
            );
            expect(cartItem.itemName).toEqual(item.itemName);
            expect(cartItem.itemPrice).toEqual(item.itemPrice);
            expect(cartItem.quantity).toEqual("1");
          }
        });

        /*
         * This is a test case to verify that the item's total price is correct in the order summary page.
         */
        it("shows the correct item total", async () => {
          const itemTotal = await getItemTotal(OrderSummaryPage.itemTotal);
          const expectedItemTotal = selectedItems.reduce(
            (acc, item) => parseFloat(item.itemPrice.slice(1)) + acc,
            0
          );
          await expect(itemTotal).toBeCloseTo(expectedItemTotal);
        });

        /*
         * This is a test case to verify that the total tax calculated is 8% of item's total price in the order summary page.
         */
        it("shows the correct calculated tax", async () => {
          const tax = await getTax(OrderSummaryPage.tax);
          const expectedTaxTotal = selectedItems.reduce(
            (acc, item) =>
              parseFloat(item.itemPrice.slice(1)) * TAX_PERCENT + acc,
            0
          );
          await expect(tax).toBeCloseTo(expectedTaxTotal);
        });

        /*
         * This is a test case to verify that the total price is correct in the order summary page.
         */
        it("shows the correct cart total", async () => {
          const total = await getTotal(OrderSummaryPage.total);
          const expectedTotal = selectedItems.reduce(
            (acc, item) =>
              parseFloat(item.itemPrice.slice(1)) +
              parseFloat(item.itemPrice.slice(1)) * TAX_PERCENT +
              acc,
            0
          );
          await expect(total).toBeCloseTo(expectedTotal);
        });

        /*
         * This is a test case to verify that when the user clicks on cancel button, it goes back to the inventory page.
         */
        describe("and user clicks on cancel", () => {
          before("click cancel", async () => {
            await OrderSummaryPage.cancel();
          });

          it("goes back to inventory page", async () => {
            await expect(InventoryPage.inventoryItems).toBeDisplayed();
          });
        });

        after("go back to inventory page and reset cart", async () => {
          await InventoryPage.open();
          for (const item of selectedItems) {
            await InventoryPage.removeFromCart(item.canonicalName);
          }
        });
      });
    });
  });
});
