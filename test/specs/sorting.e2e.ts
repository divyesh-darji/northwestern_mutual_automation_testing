import LoginPage from "../pageobjects/login.page.js";
import InventoryPage from "../pageobjects/inventory.page.js";

const getInventoryItems = async () => {
  const inventoryItems = await InventoryPage.inventoryItems;

  const items = await Promise.all(
    inventoryItems.map(async (item) => ({
      itemName: await item.$(".inventory_item_name").getText(),
      itemPrice: await item.$(".inventory_item_price").getText(),
    }))
  );

  return items;
};

const itemObjList = [
  {
    itemName: "Sauce Labs Backpack",
    itemPrice: "$29.99",
  },
  {
    itemName: "Sauce Labs Bike Light",
    itemPrice: "$9.99",
  },
  {
    itemName: "Sauce Labs Bolt T-Shirt",
    itemPrice: "$15.99",
  },
  {
    itemName: "Sauce Labs Fleece Jacket",
    itemPrice: "$49.99",
  },
  {
    itemName: "Sauce Labs Onesie",
    itemPrice: "$7.99",
  },
  {
    itemName: "Test.allTheThings() T-Shirt (Red)",
    itemPrice: "$15.99",
  },
];

/*
 * These are test cases for sorting the products on the inventory page.
 */
describe("sorting", () => {
  describe("when user opens inventory page", () => {
    beforeEach("doLogin", async () => {
      await LoginPage.open();

      await LoginPage.login(
        process.env.STANDARD_USER_NAME,
        process.env.STANDARD_USER_PASSWORD
      );

      await InventoryPage.open();
    });

    /*
     * This is a test case to verify that sorting dropdown is displayed.
     */
    it("should show sorting dropdown", async () => {
      await expect(InventoryPage.sortDropdown).toBeExisting();
    });

    /*
     * This is a test case to verify that the products are sorted bydefault in an alphabetical order.
     */
    it("should show `a to z` sorting by default", async () => {
      await expect(InventoryPage.sortDropdown).toHaveValue("az");
    });

    /*
     * This is a test case to verify that the products are sorted in an alphabetical order when the user clicks on Name(A to Z).
     */
    it("should sort items in ascending order of name", async () => {
      const items = await getInventoryItems();
      expect(items).toStrictEqual(
        [...itemObjList].sort((a, b) => a.itemName.localeCompare(b.itemName))
      );
    });

    /*
     * This is a test case to verify that the products are sorted in an descending alphabetical order when the user clicks on Name(Z to A).
     */
    describe("user selects `z to a` sorting", () => {
      beforeEach(async () => {
        await InventoryPage.sortDropdown.selectByAttribute("value", "za");
      });
      it("should show `z to a` sorting", async () => {
        await expect(InventoryPage.sortDropdown).toHaveValue("za");
      });
      it("should sort items in descending order of name", async () => {
        const items = await getInventoryItems();

        expect(items).toStrictEqual(
          [...itemObjList].sort((a, b) => b.itemName.localeCompare(a.itemName))
        );
      });
    });

    /*
     * This is a test case to verify that the products are sorted in an ascending order by price when the user clicks on Price(Low to High).
     */
    describe("user selects `price (low to high)` sorting", () => {
      beforeEach(async () => {
        await InventoryPage.sortDropdown.selectByAttribute("value", "lohi");
      });
      it("should show `price (low to high)` sorting", async () => {
        await expect(InventoryPage.sortDropdown).toHaveValue("lohi");
      });
      it("should sort items in ascending order of price", async () => {
        const items = await getInventoryItems();

        expect(items).toStrictEqual(
          [...itemObjList].sort((a, b) => {
            return (
              parseFloat(a.itemPrice.slice(1)) -
              parseFloat(b.itemPrice.slice(1))
            );
          })
        );
      });
    });

    /*
     * This is a test case to verify that the products are sorted in a descending order by price when the user clicks on Price(High to Low).
     */
    describe("user selects `price (high to low)` sorting", () => {
      beforeEach(async () => {
        await InventoryPage.sortDropdown.selectByAttribute("value", "hilo");
      });
      it("should show `price (high to low)` sorting", async () => {
        await expect(InventoryPage.sortDropdown).toHaveValue("hilo");
      });
      it("should sort items in descending order of price", async () => {
        const items = await getInventoryItems();

        expect(items).toStrictEqual(
          [...itemObjList].sort((a, b) => {
            return (
              parseFloat(b.itemPrice.slice(1)) -
              parseFloat(a.itemPrice.slice(1))
            );
          })
        );
      });
    });
  });
});
