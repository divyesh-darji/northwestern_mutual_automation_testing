import LoginPage from "../pageobjects/login.page.js";
import InventoryPage from "../pageobjects/inventory.page.js";

/*
 * These are test cases for the login page.
 */
describe("login", () => {
  /*
   * This is a test case when username and password field are empty.
   */
  describe("when user name and password field both are empty", () => {
    beforeEach("doLogin", async () => {
      await LoginPage.open();

      await LoginPage.login("", "");
    });
    it("should throw an error message stating that the username is required", async () => {
      await expect(LoginPage.errorMessage).toHaveText(
        "Epic sadface: Username is required"
      );
    });
  });

  /*
   * This is a test case when username is not empty and password field is empty.
   */
  describe("when user name is not empty and password field is empty", () => {
    beforeEach("doLogin", async () => {
      await LoginPage.open();

      await LoginPage.login(process.env.STANDARD_USER_NAME, "");
    });
    it("should throw an error message stating that the password is required", async () => {
      await expect(LoginPage.errorMessage).toHaveText(
        "Epic sadface: Password is required"
      );
    });
  });

  /*
   * This is a test case when standard user logs in with valid credentials.
   */
  describe("when standard user logs in with valid credentials", () => {
    beforeEach("doLogin", async () => {
      await LoginPage.open();

      await LoginPage.login(
        process.env.STANDARD_USER_NAME,
        process.env.STANDARD_USER_PASSWORD
      );
    });
    it("should successfully open inventory page", async () => {
      await expect(InventoryPage.inventoryHeader).toBeExisting();
      await expect(InventoryPage.inventoryItems).toBeElementsArrayOfSize(6);
    });
  });

  /*
   * This is a test case when locked user logs in with valid credentials.
   */
  describe("when locked user logs in with valid credentials", () => {
    beforeEach("doLogin", async () => {
      await LoginPage.open();

      await LoginPage.login(
        process.env.LOCKED_OUT_USER_NAME,
        process.env.LOCKED_OUT_USER_PASSWORD
      );
    });
    it("should throw an error message stating that the user is locked in the system", async () => {
      await expect(LoginPage.errorMessage).toHaveText(
        "Epic sadface: Sorry, this user has been locked out."
      );
    });
  });

  /*
   * This is a test case when invalid user logs in with invalid credentials.
   */
  describe("when invalid user logs in with invalid credentials", () => {
    beforeEach("doLogin", async () => {
      await LoginPage.open();

      await LoginPage.login(
        process.env.INVALID_USER_NAME,
        process.env.INVALID_USER_PASSWORD
      );
    });
    it("should throw an error message stating that the username and password do not match any user in this service", async () => {
      await expect(LoginPage.errorMessage).toHaveText(
        "Epic sadface: Username and password do not match any user in this service"
      );
    });
  });
});
