import Page from "./page.js";

/**
 * sub page containing specific selectors and methods for a specific page
 */
class OrderSummaryPage extends Page {
  public get cartItems() {
    return $$(".cart_item");
  }

  public get itemTotal() {
    return $(".summary_subtotal_label");
  }

  public get tax() {
    return $(".summary_tax_label");
  }

  public get total() {
    return $(".summary_total_label");
  }

  public get cancelBtn() {
    return $("button[data-test=cancel]");
  }

  public async cancel() {
    await this.cancelBtn.click();
  }

  public open() {
    return super.open("checkout-step-two.html");
  }
}

export default new OrderSummaryPage();
