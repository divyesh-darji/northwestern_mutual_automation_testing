import Page from "./page.js";

/**
 * sub page containing specific selectors and methods for a specific page
 */
class InventoryPage extends Page {
  public get inventoryHeader() {
    return $(".title=Products");
  }

  public get inventoryItems() {
    return $$(".inventory_item");
  }

  public get sortDropdown() {
    return $("select[data-test=product_sort_container]");
  }

  public open() {
    return super.open("inventory.html");
  }

  public async addItemToCart(itemName: string) {
    const addToCardBtn = await $(`button[data-test=add-to-cart-${itemName}]`);
    await addToCardBtn.click();
  }

  public async removeFromCart(itemName: string) {
    const removeBtn = await $(`button[data-test=remove-${itemName}]`);
    await removeBtn.click();
  }
}

export default new InventoryPage();
