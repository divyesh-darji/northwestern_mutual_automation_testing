import Page from "./page.js";

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ProductDescriptionPage extends Page {
  public get productDescriptionTitle() {
    return $(".inventory_details_name");
  }

  public async addItemToCart(itemName: string) {
    const addToCardBtn = await $(`button[data-test=add-to-cart-${itemName}]`);
    await addToCardBtn.click();
  }

  public async removeFromCart(itemName: string) {
    const removeBtn = await $(`button[data-test=remove-${itemName}]`);
    await removeBtn.click();
  }

  public get backToProductsBtn() {
    return $("button[data-test=back-to-products]");
  }

  public async backToProducts() {
    await this.backToProductsBtn.click();
  }

  public openById(id: string) {
    return super.open(`inventory-item.html?id=${id}`);
  }
}

export default new ProductDescriptionPage();
