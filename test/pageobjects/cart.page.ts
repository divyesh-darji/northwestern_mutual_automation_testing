import Page from "./page.js";

/**
 * sub page containing specific selectors and methods for a specific page
 */
class CartPage extends Page {
  /**
   * define selectors using getter methods
   */
  public get productDescriptionTitle() {
    return $(".inventory_item_name");
  }

  public get cartItems() {
    return $$(".cart_item");
  }

  /**
   * overwrite specific options to adapt it to page object
   */
  public open() {
    return super.open("cart.html");
  }
}

export default new CartPage();
